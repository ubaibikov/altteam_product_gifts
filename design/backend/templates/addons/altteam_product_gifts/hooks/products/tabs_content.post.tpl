<div class="hidden" id="content_product_gifts">
    {include file="views/products/components/picker/picker.tpl"
        input_name="product_gifts_ids[]"
        item_ids=$product_gifts
        multiple=true
        view_mode="external"
        select_group_class="btn-toolbar"
    }
</div>