{assign var="product_gifts" value=fn_get_altteam_porduct_gifts($product.product_id,$auth)}

{if $product_gifts}
<div class="ty-product-block__left livak">
    <div class=" gifts">
        <div><h4 class="ty-subheader">{__("product_gifts")}</h4></div>
        {foreach $product_gifts as $_gift}
                <div class="cm-reload-{$_gift.gift_id} gift_items">
                    <div>
                        <p><a href="{"products.view?product_id=`$_gift.gift_id`"|fn_url}">{$_gift.gift_product_name}</a></p>
                        <input type="hidden" name="no_cache" value="no_cache" />
                        <a href="{"products.view?product_id=`$_gift.gift_id`"|fn_url}">
                            {include 
                                file="common/image.tpl"
                                obj_id=$_gift.gift_id
                                image_width=70
                                image_height=70
                                images=$_gift.gift_main_pair
                        }</a>
                        </div>
                        <div class="gift_radio">
                        <input name="q1" type="radio" value="{$_gift.gift_id}" class="gifts_pomp" id="gift_{$_gift.gift_id}" />
                    </div>
                </div>
        {/foreach}
    </div>
</div>
{/if}

<style>

    .gifts{
            position:absolute;
            z-index:20;
            width:100%;
    }
    
    .gift_items{
        float:left;
        padding-top:10px;
    }
</style>

{*  Product gift product_data ... *}
<script>
(function (_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        $(".gifts_pomp").click(function(){
            $("#product_gift_id").val($(this).val())
        });
    })
})(Tygh, Tygh.$);
</script>