<?php


if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'post_add_to_cart',
    'get_cart_product_data_post',
    'delete_cart_product',
    'generate_cart_id'
);

