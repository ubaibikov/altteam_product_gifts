<?php

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'update') {

        $product_id = isset($_REQUEST['product_id']) ? (int) $_REQUEST['product_id'] : 0;

        $product_gifts_ids = isset($_REQUEST['product_gifts_ids']) ? array_filter((array) $_REQUEST['product_gifts_ids']) : [];

        if ($product_id) {
            fn_altteam_porduct_gifts_update_products($product_id, $product_gifts_ids);
        }
    }
}

if ($mode == 'update') {

    $product_id = empty($_REQUEST['product_id']) ? 0 : intval($_REQUEST['product_id']);

    Registry::set('navigation.tabs.product_gifts', array(
        'title' => __('product_gifts'),
        'js' => true
    ));

    $product_gifts = db_get_fields('SELECT product_gift_id FROM ?:product_gifts WHERE product_id = ?i', $product_id);

    Tygh::$app['view']->assign('product_gifts', $product_gifts);
}
