<?php

use Tygh\Tygh;

defined('BOOTSTRAP') or die('Access denied');


/** @var array $cart */
$cart = &Tygh::$app['session']['cart'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == 'add') {

        if (!empty($_REQUEST['product_data'])) {
            $product_data = [];
            foreach ($_REQUEST['product_data'] as $key => $data) {
                if (!empty($data['product_gift'])) {

                    $product_data[$data['product_gift']] = [
                        'product_id' => $data['product_gift'],
                        'amount' => 1,
                        'gift_id' =>  $data['product_gift'],
                        'gift_to_product_id' => $data['product_id'],
                        'extra' => [
                            'gift_id' =>  $data['product_gift'],
                            'gift_to_product_id' => $data['product_id'],
                        ]
                    ];

                    if (!empty($cart['gifts']['products'][$data['product_id']])) {
                        fn_delete_cart_product($cart, $cart['gifts']['gift_keys'][$data['product_id']]);
                    }

                    fn_add_product_to_cart($product_data, $cart, $auth);
                }
            }
        }
    }
}
