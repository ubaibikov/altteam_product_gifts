<?php

use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}


function fn_altteam_porduct_gifts_update_products($product_id, array $product_gifts_ids = [])
{
    db_query('DELETE FROM ?:product_gifts WHERE product_id = ?i', $product_id);

    $data_list = [];

    foreach ($product_gifts_ids as $product_gift_id) {
        if ($product_gift_id == $product_id) {
            continue;
        }

        $data_list[] = [
            'product_id' => $product_id,
            'product_gift_id' => $product_gift_id
        ];
    }

    if ($data_list) {
        db_query('INSERT INTO ?:product_gifts ?m', $data_list);
    }
}

function fn_altteam_product_gifts_post_add_to_cart(&$product_data, &$cart, $auth, $update, &$ids)
{
    foreach ($product_data as $_pdata) {
        if (!empty($_pdata['gift_id'])) {
            foreach ($ids as $_id => $product_id) {

                $cart['products'][$_id]['gifts']['gift_id'] = $_pdata['gift_id'];
                $cart['products'][$_id]['gifts']['gift_to_product_id'] = $_pdata['gift_to_product_id'];

                $cart['gifts']['products'][$_pdata['gift_to_product_id']] = $_pdata['gift_id'];
                $cart['gifts']['gift_keys'][$_pdata['gift_to_product_id']] = $_id;
            }
        }
    }
}

function fn_altteam_product_gifts_get_cart_product_data_post(&$hash, &$product, $skip_promotion, &$cart, $auth, $promotion_amount, &$_pdata, $lang_code)
{
    if (!empty($cart['products'][$hash]['gifts']['gift_id'])) {
        if ($_pdata['amount'] == 1) {
            $_pdata['price'] = 0;
        }

        $_pdata['gift_id'] = $cart['products'][$hash]['gifts']['gift_id'];
        $_pdata['gift_to_product_id'] = $cart['products'][$hash]['gifts']['gift_to_product_id'];
    }
}

function fn_altteam_product_gifts_delete_cart_product(&$cart, &$cart_id, $full_erase)
{
    if(!empty($cart['products'][$cart_id]['product_id'])){
        $cart_product_id = $cart['products'][$cart_id]['product_id'];

        if (!empty($cart_id) && !empty($cart['products'][$cart_id])) {
    
            if (!empty($cart['gifts']['gift_keys'][$cart_product_id])) {
                unset($cart['products'][$cart['gifts']['gift_keys'][$cart_product_id]]);
            }
        }     
    }
   
}

function fn_altteam_product_gifts_generate_cart_id(&$_cid, &$extra, &$only_selectable)
{
    if (!empty($extra['gift_id']) && !empty($extra['gift_to_product_id'])) {
        $_cid[] = $extra['gift_id'];
        $_cid[] = $extra['gift_to_product_id'];
    }
}

function fn_get_altteam_porduct_gifts(int $product_id, $auth)
{
    $gifts_data = db_get_array('SELECT product_gift_id FROM ?:product_gifts WHERE product_id = ?i', $product_id);

    $product_gifts = [];

    if (!empty($gifts_data)) {
        foreach ($gifts_data as $_gift_data) {
            $product_gifts[] = [
                'gift_obj_id' => $product_id . '_' . $_gift_data['product_gift_id'],
                'gift_id' => $_gift_data['product_gift_id'],
                'gift_product_name' => fn_get_product_name($_gift_data['product_gift_id']),
                'gift_main_pair' => fn_get_product_data($_gift_data['product_gift_id'], $auth)['main_pair']
            ];
        }
        return $product_gifts;
    }

    return;
}
